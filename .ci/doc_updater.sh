#!/bin/sh

set -e

MARKDOWN_FILE=${CI_PROJECT_DIR}/_ami_deployment.md
DOCS_PROJECT_ID=2269914

function replace_ami() {
  REGION=$1
  AMI=$2
  sed -i "/${REGION}/s/ami-[a-z0-9]\{8\}/$AMI/g" $MARKDOWN_FILE
}

function create_branch() {
  curl --request POST --header "PRIVATE-TOKEN: ${NORAD_ASSISTANT_API_KEY}" "https://gitlab.com/api/v4/projects/${DOCS_PROJECT_ID}/repository/branches?branch=${1}&ref=master"
}

function create_commit() {
  CONTENTS=$(cat $MARKDOWN_FILE)
  PAYLOAD=$(cat << JSON
  {
    "branch": "${1}",
    "commit_message": "Update AMI IDs for Relay",
    "actions": [
      {
        "action": "update",
        "file_path": "source/includes/relay/_ami_deployment.md",
        "encoding": "base64",
        "content": "$(base64 $MARKDOWN_FILE | tr -d '\n')"
      }
    ]
  }
JSON
  )
  curl --request POST --header "PRIVATE-TOKEN: ${NORAD_ASSISTANT_API_KEY}" --header "Content-Type: application/json" --data "${PAYLOAD}" https://gitlab.com/api/v4/projects/${DOCS_PROJECT_ID}/repository/commits
}

function create_merge_request() {
  PAYLOAD=$(cat << JSON
  {
    "source_branch": "${1}",
    "target_branch": "master",
    "title": "Update docs with latest AMI IDs",
    "description": "Verify the Relay AMI build was successful before merging.",
    "remove_source_branch": true
  }
JSON
  )
  curl --request POST --header "PRIVATE-TOKEN: ${NORAD_ASSISTANT_API_KEY}" --header "Content-Type: application/json" --data "${PAYLOAD}" https://gitlab.com/api/v4/projects/${DOCS_PROJECT_ID}/merge_requests
}

rm -f $MARKDOWN_FILE
curl https://gitlab.com/norad/docs/raw/master/source/includes/relay/_ami_deployment.md -o $MARKDOWN_FILE

AMI_JSON=$(jq -s '.[] | .builds | .[] | .artifact_id' ./packer-manifest.json | sed 's/"//g')
echo $AMI_JSON
EAST1=$(echo $AMI_JSON | cut -d, -f1 | cut -d: -f2)
EAST2=$(echo $AMI_JSON | cut -d, -f2 | cut -d: -f2)
WEST1=$(echo $AMI_JSON | cut -d, -f3 | cut -d: -f2)
WEST2=$(echo $AMI_JSON | cut -d, -f4 | cut -d: -f2)

replace_ami "us-east-1" $EAST1
replace_ami "us-east-2" $EAST2
replace_ami "us-west-1" $WEST1
replace_ami "us-west-2" $WEST2

BRANCH_NAME=update-docs-${RANDOM}
create_branch $BRANCH_NAME
create_commit $BRANCH_NAME
create_merge_request $BRANCH_NAME
