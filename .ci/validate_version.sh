#!/bin/sh

set -e

RELAY_VERSION_FILE=${CI_PROJECT_DIR}/gem_source/lib/relay/version.rb
GEMFILE_VERSION_FILE=${CI_PROJECT_DIR}/gem_source/Gemfile.lock

RELAY_VERSION=`grep -o 'VERSION = .*' ${RELAY_VERSION_FILE} | grep -Eo '\d+\.\d+\.\d+'`
GEMFILE_VERSION=`grep -o 'relay (.*)' ${GEMFILE_VERSION_FILE} | grep -Eo '\d+\.\d+\.\d+'`

if [ "$RELAY_VERSION" == "$GEMFILE_VERSION" ]
then exit 0
else
  echo "\
Relay VERSION set to ${RELAY_VERSION} while Gemfile.lock specifies ${GEMFILE_VERSION}.
Please build the gem locally and commit the updated Gemfile.lock so these match.\
"
  exit 1
fi
