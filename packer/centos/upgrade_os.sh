#! /bin/bash
function outline {
  echo
  echo --------------------------------------------------------------------------------
  echo "$@"
  echo --------------------------------------------------------------------------------
}

# Set up PPAS
outline "Adding docker GPG key"
sudo rpm --import https://download.docker.com/linux/centos/gpg

outline "Adding docker yum repo"
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

outline "Update yum repos and OS"
sudo yum makecache fast
sudo yum -y update

outline "Rebooting system"
sudo reboot
