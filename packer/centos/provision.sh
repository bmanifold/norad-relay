#! /bin/bash
function install {
  sudo yum install -y "$@"
}

function outline {
  echo
  echo --------------------------------------------------------------------------------
  echo "$@"
  echo --------------------------------------------------------------------------------
}

outline "Updating yum repo"
sudo yum makecache fast

# Install Utilities
outline "Installing utilities"
install docker-ce \
        bind-utils \
	telnet \
	nc \
        yum-utils \
        device-mapper-persistent-data \
        lvm2 \
        ntp \
        ntdate \
	vim \
        emacs
