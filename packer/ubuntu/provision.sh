#! /bin/bash
function install {
  sudo apt-get install -y --no-install-recommends "$@"
}

function outline {
  echo
  echo --------------------------------------------------------------------------------
  echo "$@"
  echo --------------------------------------------------------------------------------
}

# This image may be old, and unattended upgrades may be trying to upgade it. Let's not let it
sudo systemctl stop apt-daily.service
sudo systemctl stop apt-daily.timer

outline "Updating apt repo and removing unneccessary packages"
sudo apt-get update
sudo apt-get -y autoremove

# Install Utilities
outline "Installing utilities"
install docker-ce=17.06.1~ce-0~ubuntu \
        dnsutils \
	iputils-ping \
        ntp \
	vim \
        emacs
