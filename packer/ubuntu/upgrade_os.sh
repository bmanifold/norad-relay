#! /bin/bash
function outline {
  echo
  echo --------------------------------------------------------------------------------
  echo "$@"
  echo --------------------------------------------------------------------------------
}

# This image may be old, and unattended upgrades may be trying to upgade it. Let's not let it
sudo systemctl stop apt-daily.service
sudo systemctl stop apt-daily.timer

# Set up PPAS
outline "Adding docker apt-key"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

outline "Adding docker apt repo"
sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

outline "Update apt repos and OS"
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confnew" dist-upgrade -y

outline "Rebooting system"
sudo reboot
