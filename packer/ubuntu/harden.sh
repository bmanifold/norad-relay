#! /bin/bash

# Execute on server to be hardened. Should only be run once!

for script in $(ls $HOME/provision/hardening-scripts/*.sh)
do
  echo "Running ${script}..."
  sudo ${script}
done
