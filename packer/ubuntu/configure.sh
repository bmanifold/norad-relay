#! /bin/bash
function outline {
  echo
  echo --------------------------------------------------------------------------------
  echo "$@"
  echo --------------------------------------------------------------------------------
}

outline "Configure the Docker daemon and service"
# Install Docker and Docker Compose
sudo groupadd docker

# We will use the daemon.json file to configure our Docker daemon, so first we need to drop in a new SystemD exec to not throw any flags
sudo mkdir /etc/systemd/system/docker.service.d
sudo mv $HOME/provision/systemd/docker.conf /etc/systemd/system/docker.service.d/.
sudo chown -R root:root /etc/systemd/system/docker.service.d

# Put the config file in place now
sudo mv $HOME/provision/docker/daemon.json /etc/docker/daemon.json
sudo chown -R root:root /etc/docker/daemon.json

sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker

COMPOSE_VERSION="1.15.0"
outline "Install Docker Compose ${COMPOSE_VERSION}"
sudo curl -L "https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chown root:root /usr/local/bin/docker-compose
sudo chmod ug+x /usr/local/bin/docker-compose

# Enable the norad-relay systemd unit
RELAY_UNIT=norad-relay.service
outline "Set up the Norad Relay service"
sudo mv $HOME/provision/systemd/${RELAY_UNIT} /etc/systemd/system/.
sudo chown root:root /etc/systemd/system/${RELAY_UNIT}
sudo systemctl enable ${RELAY_UNIT}

outline "Set up the Norad Relay Org Token Retriever"
TOKEN_UNIT=norad-relay-org-token-retriever.service
sudo mv $HOME/provision/systemd/${TOKEN_UNIT} /etc/systemd/system/.
sudo chown root:root /etc/systemd/system/${TOKEN_UNIT}
sudo systemctl enable ${TOKEN_UNIT}

outline "Move Norad Relay files into place"
sudo mkdir /relay
sudo cp -r $HOME/provision/relay/* /relay
sudo chmod -R o-rwx /relay

outline "Pull Norad Relay image"
sudo docker pull norad-registry.cisco.com:5000/relay:latest

# Clean up
outline "Removing ~/provision directory"
rm -rf ~/provision

outline "Removing exited Docker containers"
sudo docker rm $(sudo docker ps -a -q -f status=exited)

outline "Removing dangling docker images"
sudo docker rmi $(sudo docker images -q -f dangling=true)

outline "Clearing Bash history"
cat /dev/null > ~/.bash_history
history -c
