#! /bin/bash

sed -i '/^#\s*auth\s\+required\s\+pam_wheel.so\s*$/s/^#\s*//' /etc/pam.d/su
sed -i '/\s*auth\s\+required\s\+pam_wheel.so\s*$/s/$/ use_uid/' /etc/pam.d/su

groupadd wheel
usermod -a -G wheel root
