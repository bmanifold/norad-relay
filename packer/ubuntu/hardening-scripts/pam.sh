#! /bin/bash

apt-get install -y --no-install-recommends libpam-pwquality

echo "" | cat >> /etc/pam.d/common-password
echo "# Enforce CIS Password Quality Rules" | cat >> /etc/pam.d/common-password
echo "password requisite pam_pwquality.so try_first_pass retry=3" | cat >> /etc/pam.d/common-password
echo "password sufficient pam_unix.so remember=5" | cat >> /etc/pam.d/common-password


function pw_quality_rule {
  echo "$@" | cat >> /etc/security/pwquality.conf
}

echo "" | cat >> /etc/security/pwquality.conf
echo "# Enforce CIS Password Quality Rules" | cat >> /etc/security/pwquality.conf
pw_quality_rule minlen=14
pw_quality_rule dcredit=-1
pw_quality_rule ucredit=-1
pw_quality_rule ocredit=-1
pw_quality_rule lcredit=-1
