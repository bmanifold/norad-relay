#! /bin/bash

function set_param {
  echo "$@" >> /etc/sysctl.d/98-cis_network_parameters.conf
}

set_param net.ipv4.conf.all.send_redirects = 0
set_param net.ipv4.conf.default.send_redirects = 0
set_param net.ipv4.conf.all.accept_source_route = 0
set_param net.ipv4.conf.default.accept_source_route = 0
set_param net.ipv4.conf.all.accept_redirects = 0
set_param net.ipv4.conf.default.accept_redirects = 0
set_param net.ipv4.conf.all.secure_redirects = 0
set_param net.ipv4.conf.default.secure_redirects = 0
set_param net.ipv4.conf.all.log_martians = 1
set_param net.ipv4.conf.default.log_martians = 1

set_param net.ipv6.conf.all.accept_ra = 0
set_param net.ipv6.conf.all.accept_redirects = 0
