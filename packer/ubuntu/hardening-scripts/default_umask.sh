#! /bin/bash

echo "" | cat >> /etc/profile
echo "# CIS Recommended Default umask" | cat >> /etc/profile
echo "umask 027" | cat >> /etc/profile

echo "" | cat >> /etc/bash.bashrc
echo "# CIS Recommended Default umask" | cat >> /etc/bash.bashrc
echo "umask 027" | cat >> /etc/bash.bashrc

echo "Touch .bash_history to make sure it exists to set proper perms"
touch ~ubuntu/.bash_history
chown ubuntu:ubuntu ~ubuntu/.bash_history

echo "Ensure all dot files are not group writable"
chmod go-w ~ubuntu/.*
