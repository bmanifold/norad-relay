#! /bin/bash

harden_ssh () {
  match_pattern=$1
  full_string=$2

  if [[ $(grep -e "$match_pattern" /etc/ssh/sshd_config) ]]; then
    sed -i "/${match_pattern}/s/${match_pattern}/${full_string}/" /etc/ssh/sshd_config
  else
    echo "${full_string}" | cat >> /etc/ssh/sshd_config
  fi
}

harden_ssh "^\s*LoginGraceTime\s\+[[:digit:]]\+.*" "LoginGraceTime 60"
harden_ssh "^\s*PermitRootLogin\s\+.*" "PermitRootLogin no"
harden_ssh "^\s*Protocol\s\+[[:digit:]]\+.*" "Protocol 2"
harden_ssh "^\s*LogLevel\s.*" "LogLevel INFO"
harden_ssh "^\s*X11Forwarding\s.*" "X11Forwarding no"
harden_ssh "^\s*MaxAuthTries\s.*" "MaxAuthTries 4"
harden_ssh "^\s*IgnoreRhosts\s.*" "IgnoreRhosts yes"
harden_ssh "^\s*HostbasedAuthentication\s.*" "HostbasedAuthentication no"
harden_ssh "^\s*PermitEmptyPasswords\s.*" "PermitEmptyPasswords no"
harden_ssh "^\s*PermitUserEnvironment\s.*" "PermitUserEnvironment no"
harden_ssh "^\s*MACs\s.*" "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com"
harden_ssh "^\s*ClientAliveInterval\s.*" "ClientAliveInterval 300"
harden_ssh "^\s*ClientAliveCountMax\s.*" "ClientAliveCountMax 0"
harden_ssh "^\s*AllowUsers\s.*" "AllowUsers ubuntu"
harden_ssh "^\s*Banner\s.*" "Banner /etc/issue.net"

chmod 600 /etc/ssh/sshd_config
