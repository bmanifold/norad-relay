#! /bin/bash

echo "* hard core 0" > /etc/security/limits.d/cis_limits.conf
echo "fs.suid_dumpable = 0"  > /etc/sysctl.d/98-cis_suid_dumpable.conf

# apport overides the suid_dumpable setting
apt-get purge -y apport
