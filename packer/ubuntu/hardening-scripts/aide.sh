#! /bin/bash

DEBIAN_FRONTEND=noninteractive apt-get install -y aide nullmailer

# Set up our own cron job. Remove the one in /etc/cron.daily
rm -f /etc/cron.daily/aide
echo "0 5 * * * /usr/bin/aide  --check" > /tmp/aide.crontab
crontab /tmp/aide.crontab
rm /tmp/aide.crontab
