#! /bin/bash

function disable_mod {
  echo "install "$@" /bin/true" | cat >> /etc/modprobe.d/CIS.conf
}

disable_mod tipc
disable_mod sctp
disable_mod dccp

# Filesystems
disable_mod cramfs
disable_mod freevxfs
disable_mod jffs2
disable_mod hfs
disable_mod hfsplus
disable_mod squashfs
disable_mod udf
disable_mod vfat
