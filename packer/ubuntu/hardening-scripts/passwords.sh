echo 'Lock account if password has been expird longer than 30 days'
useradd -D -f 30

echo 'Set minimum number of days between password changes'
sed -i 's/^\s*PASS_MIN_DAYS\s\+[[:digit:]]\+/PASS_MIN_DAYS 99/' /etc/login.defs

echo 'Set maximum password lifetime'
sed -i 's/^\s*PASS_MAX_DAYS\s\+[[:digit:]]\+/PASS_MAX_DAYS 90/' /etc/login.defs
