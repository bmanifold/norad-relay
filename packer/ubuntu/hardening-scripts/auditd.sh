#! /bin/bash

apt-get install -y auditd
sed -i 's/space_left_action\s*=\s*.*/space_left_action = email/' /etc/audit/auditd.conf
sed -i 's/admin_space_left_action\s*=\s*.*/admin_space_left_action = halt/' /etc/audit/auditd.conf
sed -i 's/max_log_file_action\s*=\s*.*/max_log_file_action = keep_logs/' /etc/audit/auditd.conf

RULES_FILE=/etc/audit/audit.rules
echo "-a always,exit -F arch=b64 -S adjtimex -S settimeofday -k time-change" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S adjtimex -S settimeofday -S stime -k time-change" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S clock_settime -k time-change" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S clock_settime -k time-change" >> $RULES_FILE
echo "-w /etc/localtime -p wa -k time-change" >> $RULES_FILE
echo "-w /etc/group -p wa -k identity" >> $RULES_FILE
echo "-w /etc/passwd -p wa -k identity" >> $RULES_FILE
echo "-w /etc/gshadow -p wa -k identity" >> $RULES_FILE
echo "-w /etc/shadow -p wa -k identity" >> $RULES_FILE
echo "-w /etc/security/opasswd -p wa -k identity" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S sethostname -S setdomainname -k system-locale" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S sethostname -S setdomainname -k system-locale" >> $RULES_FILE
echo "-w /etc/issue -p wa -k system-locale" >> $RULES_FILE
echo "-w /etc/issue.net -p wa -k system-locale" >> $RULES_FILE
echo "-w /etc/hosts -p wa -k system-locale" >> $RULES_FILE
echo "-w /etc/network -p wa -k system-locale" >> $RULES_FILE
echo "-w /etc/networks -p wa -k system-locale" >> $RULES_FILE
echo "-w /etc/apparmor/ -p wa -k MAC-policy" >> $RULES_FILE
echo "-w /etc/apparmor.d/ -p wa -k MAC-policy" >> $RULES_FILE
echo "-w /var/log/faillog -p wa -k logins" >> $RULES_FILE
echo "-w /var/log/lastlog -p wa -k logins" >> $RULES_FILE
echo "-w /var/log/tallylog -p wa -k logins" >> $RULES_FILE
echo "-w /var/run/utmp -p wa -k session" >> $RULES_FILE
echo "-w /var/log/wtmp -p wa -k session" >> $RULES_FILE
echo "-w /var/log/btmp -p wa -k session" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete" >> $RULES_FILE
echo "-w /etc/sudoers -p wa -k scope" >> $RULES_FILE
echo "-w /etc/sudoers.d -p wa -k scope" >> $RULES_FILE
echo "-w /var/log/sudo.log -p wa -k actions" >> $RULES_FILE
echo "-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts" >> $RULES_FILE
echo "-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts" >> $RULES_FILE


# Must be last!
echo "-e 2" >> $RULES_FILE
