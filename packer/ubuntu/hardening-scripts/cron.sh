#! /bin/bash

function harden_cron_file {
  chown root:root "$@"
  chmod og-rwx "$@"
}

harden_cron_file /etc/crontab
harden_cron_file /etc/cron.hourly
harden_cron_file /etc/cron.daily
harden_cron_file /etc/cron.weekly
harden_cron_file /etc/cron.monthly
harden_cron_file /etc/cron.d

# Restrict access to creating cron jobs
rm -f /etc/at.deny
rm -f /etc/cron.deny
touch /etc/cron.allow
touch /etc/at.allow
chmod og-rwx /etc/cron.allow
chmod og-rwx /etc/at.allow
chown root:root /etc/cron.allow
chown root:root /etc/at.allow
