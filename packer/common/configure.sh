#! /bin/bash
function outline {
  echo
  echo --------------------------------------------------------------------------------
  echo "$@"
  echo --------------------------------------------------------------------------------
}

outline "Configure the Docker daemon and service"
# Install Docker and Docker Compose
sudo groupadd docker

# We will use the daemon.json file to configure our Docker daemon, so first we need to drop in a new SystemD exec to not throw any flags
sudo mkdir /etc/systemd/system/docker.service.d
sudo mv $HOME/provision/systemd/docker.conf /etc/systemd/system/docker.service.d/.
sudo chown -R root:root /etc/systemd/system/docker.service.d

sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker

outline "Install Relay CLI Management Utility"
curl -L -o relay https://gitlab.com/norad/relay-cli/-/jobs/artifacts/master/raw/build/relay?job=build_linux_amd64
sudo chown root:root relay
sudo chmod 770 relay
sudo mv relay /usr/local/bin/relay

outline "Set up the Norad Relay Org Token Retriever"
sudo cp -r $HOME/provision/relay/token_retriever.sh /usr/local/bin/token_retriever.sh
chmod 770 /usr/local/bin/token_retriever.sh
TOKEN_UNIT=norad-relay-org-token-retriever.service
sudo mv $HOME/provision/systemd/${TOKEN_UNIT} /etc/systemd/system/.
sudo chown root:root /etc/systemd/system/${TOKEN_UNIT}
sudo systemctl enable ${TOKEN_UNIT}

outline "Pull Norad Relay image"
sudo docker pull norad-registry.cisco.com:5000/relay:latest

# Clean up
outline "Removing ~/provision directory"
rm -rf ~/provision

outline "Removing exited Docker containers"
sudo docker rm $(sudo docker ps -a -q -f status=exited)

outline "Removing dangling docker images"
sudo docker rmi $(sudo docker images -q -f dangling=true)

outline "Clearing Bash history"
cat /dev/null > ~/.bash_history
history -c
