#!/bin/bash

echo "Querying user data to retrieve organization token..."
NORAD_ORGANIZATION_TOKEN=`cat /var/lib/cloud/instance/user-data.txt | gawk 'match($0, /NORAD_ORGANIZATION_TOKEN=(.+)/, a) { print a[1] }'`
echo "Done."

if [ -z "$NORAD_ORGANIZATION_TOKEN" ]
then
  echo "Unable to retrieve organization token. Did you set it in user data?"
  exit 0 # Exit successfully so that we don't block other systemd processes
fi

export NORAD_ORGANIZATION_TOKEN=${NORAD_ORGANIZATION_TOKEN}

# This can safely be done each time this service is started. If the Relay has already been
# installed, this command will fail gracefully
echo "Installing the Norad Relay"
relay install
echo "Done."

exit 0
