# Specify Vagrant version and Vagrant API version
Vagrant.require_version ">= 1.7.0"
VAGRANTFILE_API_VERSION = "2"

# Ubuntu docker installation script
$docker_ubuntu_script = <<SCRIPT
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" >> /etc/apt/sources.list.d/docker.list
apt-get -y update
apt-get -y purge lxc-docker
apt-cache policy docker-engine
apt-get -y install linux-image-extra-$(uname -r) docker-engine
apt-get -y install python-pip
pip install docker-compose
apt-get install ruby1.9.1-dev
gem install bundler --no-ri --no-rdoc
gem install fpm --no-ri --no-rdoc
cd /vagrant
rm -f norad-relay_*.deb
./package.sh
SCRIPT

# Centos docker installation script
$docker_centos_script = <<SCRIPT
yum -y group install 'Development Tools'
tee /etc/yum.repos.d/docker.repo <<-EOF
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF
yum -y install docker-engine
systemctl start docker
curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
yum -y install ruby ruby-devel
gem install bundler --no-ri --no-rdoc
gem install fpm --no-ri --no-rdoc
cd /vagrant
rm -f norad-relay-*.rpm
PATH=$PATH:/usr/local/bin ./package.sh
SCRIPT

# Create and configure the VM(s)
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.provider "virtualbox" do |v|
    v.memory = 8192
    v.cpus = 2
  end

  config.vm.define "relay-ubuntu" do |norad|
    # Spin up a "host box" for use with the Docker provider
    # and then provision it with Docker
    norad.vm.box = 'ubuntu/trusty64'

    # Set the hostname for the machine
    norad.vm.hostname = "relay-ubuntu"

    norad.vm.provision "shell", inline: $docker_ubuntu_script
  end

  config.vm.define "relay-centos" do |norad|
    # Spin up a "host box" for use with the Docker provider
    # and then provision it with Docker
    norad.vm.box = 'centos/7'

    # Set the hostname for the machine
    norad.vm.hostname = "relay-centos"

    norad.vm.provision "shell", inline: $docker_centos_script, privileged: true
  end

end
