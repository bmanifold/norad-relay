# frozen_string_literal: true

require 'relay/api_payloads'

RSpec.describe Relay::ApiPayloads do
  let(:subject_klass) do
    Class.new do
      include Relay::ApiPayloads
    end
  end
  let(:instance) { subject_klass.new }

  before :each do
    ENV['NORAD_RELAY_CONFIG_FILE'] = "#{File.dirname(__FILE__)}/support/dummy_config.yml"
  end

  describe '#heartbeat_payload' do
    it 'returns the VERSION constant' do
      expect(instance.heartbeat_payload[:version][:current]).to eq Relay::VERSION
    end
  end

  describe '#assessment_update_payload' do
    it 'returns assessment param with :state set to :complete' do
      expect(instance.assessment_update_payload).to eq(assessment: { state: :complete })
    end
  end
end
