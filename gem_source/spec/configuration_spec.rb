# frozen_string_literal: true

require 'relay/configuration'
require 'relay/crypto_utilities'
require 'relay/docker_daemon'

describe Relay::Configuration do
  let(:default_config) do
    {
      api_uri: 'https://norad.cisco.com:8443/v1',
      queue_user: 'norad-consumer',
      queue_password: '',
      db: '/relay/relay.store',
      private_key: '/relay/relay.pem',
      organization_token: '0',
      containers_file: '/relay/containers.yml',
      file_encryption_public_key_file: '/relay/file_encryption_key.pub',
      file_encryption_aes_key_file: '/relay/file_encryption_key.enc',
      worker_pool_size: 4,
      relay_host_docker_socket: 'unix:///relay/docker.sock',
      enable_tls: true,
      whitelisted_repository_hosts: %w[norad-registry.cisco.com],
      host_containers_yml_file: '/etc/norad.d/containers.yml.enc'
    }
  end

  context 'with an empty config file' do
    describe 'the .configure! method' do
      context 'with an empty config file' do
        before :each do
          ENV['NORAD_RELAY_CONFIG_FILE'] = "#{File.dirname(__FILE__)}/support/default_config.yml"
        end

        it 'sets the @values attribute to the default config' do
          described_class.configure!
          expect(described_class.values).to eq(default_config)
        end
      end

      context 'with values specified in the config file' do
        before :each do
          ENV['NORAD_RELAY_CONFIG_FILE'] = "#{File.dirname(__FILE__)}/support/custom_config.yml"
        end

        it 'overrides the default config for the specified key' do
          described_class.configure!
          expect(described_class.values[:api_uri]).to eq('https://foo.invalid')
        end
      end
    end
  end

  describe 'the .values method' do
    before :each do
      ENV['NORAD_RELAY_CONFIG_FILE'] = "#{File.dirname(__FILE__)}/support/default_config.yml"
      described_class.configure!
    end

    it 'returns a Hash of the configuration values' do
      expect(described_class.values).to eq(default_config)
    end
  end
end
