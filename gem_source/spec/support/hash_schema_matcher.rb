# frozen_string_literal: true

# Taken from https://robots.thoughtbot.com/validating-json-schemas-with-an-rspec-matcher
require 'json-schema'
RSpec::Matchers.define :match_hash_schema do |schema|
  match do |input_hash|
    schema_directory = "#{Dir.pwd}/spec/support/schemas"
    schema_path = "#{schema_directory}/#{schema}.json"
    JSON::Validator.validate!(schema_path, input_hash, strict: false)
  end
end
