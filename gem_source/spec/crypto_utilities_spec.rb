# frozen_string_literal: true

require 'relay/configuration'
require 'relay/crypto_utilities'
require 'securerandom'
require 'fileutils'

describe Relay::CryptoUtilities do
  describe 'class methods' do
    describe '.encrypt_containers_file' do
      let(:test_dir) { File.join(Dir.home, "rspec_test_run_dir_#{SecureRandom.hex}") }
      let(:containers_file) { "#{test_dir}/containers.yml" }
      let(:api_double) { double }

      before :each do
        Dir.mkdir(test_dir)
        allow(api_double).to receive_message_chain('fetch_details.fetch')
        allow(described_class).to receive(:save_public_key)
        Relay::Configuration.values[:containers_file] = containers_file
      end

      after :each do
        FileUtils.rm_r(test_dir)
      end

      it 'generates a stub containers.yml file if one does not exist' do
        expect(File.exist?(containers_file)).to eq(false)
        allow(File).to receive(:delete)
        allow(described_class).to receive(:encrypt_containers_file_with_aes)
        allow(described_class).to receive(:encrypt_and_save_aes_key)

        described_class.encrypt_containers_file(api_double)
        expect(File.exist?(containers_file)).to eq(true)
        config = YAML.safe_load(File.read(containers_file))
        expect(config['norad_relay_stub_config']).to_not eq(nil)
      end

      it 'encrypts the containers.yml file and saves the result' do
        expect(File.exist?(containers_file)).to eq(false)
        allow(File).to receive(:delete)
        allow(described_class).to receive(:encrypt_and_save_aes_key)

        described_class.encrypt_containers_file(api_double)
        expect(File.exist?("#{containers_file}.enc")).to eq(true)
      end

      it 'removes the plaintext file' do
        expect(File.exist?(containers_file)).to eq(false)
        allow(described_class).to receive(:encrypt_containers_file_with_aes)
        allow(described_class).to receive(:encrypt_and_save_aes_key)

        expect(File).to receive(:delete).with(containers_file)
        described_class.encrypt_containers_file(api_double)
      end

      it 'does nothing if an encrypted file is present without a corresponding plaintext version' do
        File.open("#{containers_file}.enc", 'w') { |f| f.write 'foo' }
        expect(File.exist?(containers_file)).to eq(false)
        expect(described_class).to_not receive(:generate_stub_file)
        expect(described_class.encrypt_containers_file(api_double)).to eq(nil)
      end

      it 'generates a new config if an encrypted file is present but a plaintext version exists' do
        File.open("#{containers_file}.enc", 'w') { |f| f.write 'foo' }
        File.open(containers_file, 'w') { |f| f.write({ bar: 'baz' }.to_yaml) }
        allow(described_class).to receive(:encrypt_containers_file_with_aes)
        allow(described_class).to receive(:encrypt_and_save_aes_key)

        expect(described_class).to_not receive(:generate_stub_file)
        expect(described_class).to receive(:encrypt_containers_file_with_aes)
        expect(described_class.encrypt_containers_file(api_double)).to_not eq(nil)
      end
    end
  end
end
