# frozen_string_literal: true

require 'relay/api_exceptions'

RSpec.describe do
  shared_examples_for 'custom exception message' do
    describe 'subject_klass' do
      it 'overrides #message' do
        # RSpec's expect {...}.to raise_exception was causing weirdness here and would not pass
        begin
          raise subject_klass
        rescue subject_klass => e
          expect(e).to be_a subject_klass
          expect(e.message).to eq expected_message
        end
      end
    end
  end

  [
    SocketError,
    RestClient::BadRequest,
    RestClient::NotFound,
    RestClient::UnprocessableEntity,
    RestClient::Exceptions::Timeout,
    RestClient::InternalServerError
  ].each do |klass|
    it_behaves_like 'custom exception message' do
      let(:subject_klass) { klass }
      let(:expected_message) { klass::MESSAGE }
    end
  end
end
