# frozen_string_literal: true

require 'relay/configuration'
require 'relay/crypto_utilities'
require 'relay/docker_daemon'

describe Relay::DockerDaemon do
  before(:each) do
    @api_double = double
    allow(@api_double).to receive(:complete_assessment)
  end

  context 'when credentials provided' do
    let(:msg) do
      {
        'name' => 'container_name',
        'Image' => 'foobar/image:latest',
        'Cmd' => 'Blah',
        'Env' => %w[Blah Blah],
        'Username' => 'Test_Username',
        'Password' => 'Test_Password',
        'encoded_relay_private_key' => 'fake_private_key'
      }
    end

    before(:each) do
      allow(Relay::Configuration).to receive(:values).and_return({})
      @daemon = Relay::DockerDaemon.new(@api_double)

      # stub other calls
      allow(@daemon).to receive(:start_container).and_return('a container')
      allow(@daemon).to receive(:log_container_errors)
    end

    it 'passes creds to Docker' do
      image = { fromImage: msg['Image'] }
      creds = { 'Username' => msg['Username'], 'Password' => msg['Password'] }
      expect(Docker::Image).to receive(:create).with(image, creds).and_return(:ok)

      @daemon.send_to_docker(msg)
    end

    context 'with #pull_image stubbed' do
      before(:each) do
        allow(@daemon).to receive(:pull_image)
      end

      it 'removes container if RELAY_MODE == production' do
        old_relay_mode = ENV['RELAY_MODE']
        ENV['RELAY_MODE'] = 'production'
        expect(@daemon).to receive(:remove_container).with('a container')

        @daemon.send_to_docker(msg)

        ENV['RELAY_MODE'] = old_relay_mode
      end

      it 'completes assessment if name is present in the msg' do
        expect(@api_double).to receive(:complete_assessment)
        @daemon.send_to_docker(msg)
      end

      it 'does not complete assessment if name not present in the msg' do
        expect(@api_double).not_to receive(:complete_assessment)
        msg_without_name = msg
        msg_without_name.delete('name')
        @daemon.send_to_docker(msg_without_name)
      end
    end

    it 'properly generates container_opts from msg' do
      allow(Relay::Configuration).to receive(:values)
        .and_return(host_containers_yml_file: '/relay/containers.yml')
      allow(Relay::CryptoUtilities).to receive(:retrieve_aes_key).and_return('a:aaaaaaaa/aa')
      allow(Base64).to receive(:strict_encode64).and_return('aaaaaaaaaaaa+aaaaaaaaa==')
      container_opts_return_value = @daemon.send(:container_opts, msg)
      expect(container_opts_return_value).to match_hash_schema('container_opts')
    end
  end

  describe 'class methods' do
    describe '::containers_file' do
      it 'sets returns the configured value' do
        expect(described_class.containers_file).to eq(Relay::Configuration.values[:host_containers_yml_file])
      end
    end
  end

  describe 'instance methods' do
    let(:daemon) { Relay::DockerDaemon.new(@api_double) }
    let(:image_with_digest) { 'image@sha256:be0c5cdadc7525681e511c8c258326033f96d611324ba9c2426356cc79e18b85' }
    let(:digest_list) { %W[image@sha256:deadbeef <none>:<none> <none>@<none> #{image_with_digest}] }

    before(:each) do
      allow(Relay::Configuration).to receive(:values).and_return({})
      image_list = digest_list.map do |d|
        x = double
        x_hash = { 'RepoDigests' => [d] }
        allow(x).to receive(:info).and_return(x_hash)
        expect(x).to receive(:remove) if d == image_with_digest
        x
      end
      allow(Docker::Image).to receive(:all).and_return(image_list)
    end

    describe '#remove_dangling_images' do
      it 'only calls remove if digest tag is fully formed and matches the pulled image name' do
        image_name = 'image:0.0.1'
        daemon.remove_dangling_images(image_name)
      end

      it 'handles image names without tags' do
        image_name = 'image'
        daemon.remove_dangling_images(image_name)
      end
    end
  end
end
