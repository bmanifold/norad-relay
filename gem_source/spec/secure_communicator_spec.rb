# frozen_string_literal: true

require 'relay/configuration'
require 'relay/secure_communicator'

describe Relay::SecureCommunicator do
  before :each do
    allow(Relay::Configuration).to receive(:values)
      .and_return(
        api_uri: 'https://foo.dummy',
        db: 'spec/support/relay.store',
        private_key: 'spec/support/relay.pem'
      )
  end

  describe '#key_fingerprint' do
    before :each do
      # Stub key generation to produce the same fingerprint each time
      key_path = "#{File.dirname(__FILE__)}/support/relay_priv_key.txt"
      key_txt = File.open(key_path).read
      key = OpenSSL::PKey::RSA.new key_txt
      allow(OpenSSL::PKey::RSA).to receive(:new).and_return(key)
    end

    it 'computes the SHA256 of the public key' do
      fingerprint =
        'd7:86:27:c1:89:f0:54:92:2c:ee:68:2c:22:98:cf:de:'\
        '10:d6:ff:22:82:17:8a:9c:7a:da:ee:70:3e:28:c4:05'
      expect(described_class.new.key_fingerprint).to eq fingerprint
    end
  end
end
