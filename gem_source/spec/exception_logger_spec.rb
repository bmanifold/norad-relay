# frozen_string_literal: true

require 'json'
require 'rest-client'
require 'relay/logger'
require 'relay/api_exceptions'
require 'relay/exception_logger'

RSpec.describe Relay::ExceptionLogger do
  let(:subject_klass) do
    Class.new do
      include Relay::Logger
      include Relay::ExceptionLogger
    end
  end
  let(:instance) { subject_klass.new }
  let(:logger) { instance.logger }

  before :each do
    allow(logger).to receive(:error)
  end

  def raise_our_exception(exception, opts = {})
    instance.log_exceptions(exception, opts) do
      raise exception
    end
  end

  shared_examples_for 'generic exception logging' do
    let(:expected_message) { exception_klass.to_s } # exceptions usually return their class when their message is nil

    it 'has a logger defined' do
      expect(subject_klass.new.logger).to be_a Log4r::Logger
    end

    describe '#log_exceptions' do
      it 'suppresses further raising' do
        expect(logger).to receive(:error).with(expected_message)
        expect { raise_our_exception(exception_klass, suppress: true) }.not_to raise_exception
      end
    end
  end

  describe 'JSON::ParserError' do
    it_behaves_like('generic exception logging') do
      let(:exception_klass) { JSON::ParserError }
    end
  end

  describe 'KeyError' do
    it_behaves_like 'generic exception logging' do
      let(:exception_klass) { KeyError }
    end
  end

  [
    SocketError,
    RestClient::BadRequest,
    RestClient::NotFound,
    RestClient::UnprocessableEntity,
    RestClient::Exceptions::Timeout,
    RestClient::InternalServerError
  ].each do |klass|
    it_behaves_like('generic exception logging') do
      let(:exception_klass) { klass }
      let(:expected_message) { klass.new.message }
    end
  end
end
