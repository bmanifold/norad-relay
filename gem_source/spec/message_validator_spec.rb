# frozen_string_literal: true

require 'relay/message_validator'

describe Relay::MessageValidator do
  def merge(base, docker_opts)
    base['docker_options'].merge!(docker_opts)
    base
  end

  context 'when validating image paths' do
    let(:base_msg) do
      { 'docker_options' => { 'Cmd' => 'Blah', 'Env' => 'Blah' } }
    end

    context 'for docker hub' do
      before :each do
        allow(Relay::Configuration).to receive(:values).and_return(
          whitelisted_repository_hosts: ['hub.docker.com']
        )
      end

      it 'disallows foobar.host.com/image:tag' do
        msg = merge(base_msg, 'Image' => 'foobar.host.com/image:tag')
        expect(subject.valid?(msg)).to be_falsey
      end

      it 'allows image' do
        msg = merge(base_msg, 'Image' => 'image')
        expect(subject.valid?(msg)).to be_truthy
      end

      it 'allows image:tag' do
        msg = merge(base_msg, 'Image' => 'image:tag')
        expect(subject.valid?(msg)).to be_truthy
      end
    end

    context 'for private registries' do
      before :each do
        allow(Relay::Configuration).to receive(:values).and_return(
          whitelisted_repository_hosts: ['foobar.host.com']
        )
      end

      it 'allows foobar.host.com/image:tag' do
        msg = merge(base_msg, 'Image' => 'foobar.host.com/image:tag')
        expect(subject.valid?(msg)).to be_truthy
      end

      it 'allows foobar.host.com:5000/image:tag' do
        msg = merge(base_msg, 'Image' => 'foobar.host.com:5000/image:tag')
        expect(subject.valid?(msg)).to be_truthy
      end

      it 'allows foobar.host.com:5000/image@revision' do
        msg = merge(base_msg, 'Image' => 'foobar.host.com:5000/image@revision')
        expect(subject.valid?(msg)).to be_truthy
      end

      it 'disallows foobar.host.com:5000' do
        msg = merge(base_msg, 'Image' => 'foobar.host.com:5000')
        expect(subject.valid?(msg)).to be_falsey
      end

      it 'allows foobar.host.com:5000/image' do
        msg = merge(base_msg, 'Image' => 'foobar.host.com:5000/image')
        expect(subject.valid?(msg)).to be_truthy
      end

      it 'disallows image' do
        msg = merge(base_msg, 'Image' => 'image')
        expect(subject.valid?(msg)).to be_falsey
      end
    end
  end

  context 'when validating credentials' do
    let(:base_msg) do
      {
        'docker_options' => {
          'Image' => 'foobar.co:5000/image:latest',
          'Cmd' => 'Blah',
          'Env' => 'Blah'
        }
      }
    end

    before :each do
      allow(Relay::Configuration).to receive(:values).and_return(
        whitelisted_repository_hosts: ['foobar']
      )
    end

    context 'when username and password are blank' do
      it 'is valid' do
        msg = base_msg
        expect(subject.valid?(msg)).to be_truthy
      end
    end

    context 'when username and password are provided' do
      it 'is valid' do
        msg = merge(base_msg, 'Username' => 'Test', 'Password' => 'Test')
        expect(subject.valid?(msg)).to be_truthy
      end
    end

    context 'when only username is provided' do
      it 'is invalid' do
        msg = merge(base_msg, 'Username' => 'Test')
        expect(subject.valid?(msg)).to be_falsey
      end
    end

    context 'when only password is provided' do
      it 'is invalid' do
        msg = merge(base_msg, 'Password' => 'Test')
        expect(subject.valid?(msg)).to be_falsey
      end
    end
  end
end
