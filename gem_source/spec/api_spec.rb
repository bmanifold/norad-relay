# frozen_string_literal: true

require 'relay/configuration'
require 'relay/store'
require 'relay/api'

describe Relay::Api do
  let(:api) { described_class.new }
  let(:json) { '{ "response": "valid" }' }

  before(:each) do
    allow(Relay::Configuration).to receive(:values)
      .and_return(
        private_key: 'spec/support/relay.pem',
        organization_token: 'foobar',
        db: 'spec/support/relay.store',
        api_uri: 'https://foo.invalid'
      )
    stub_const('Relay::Api::QUEUE_SLEEP', 0)
    allow_any_instance_of(described_class).to receive(:update_relay_meta_info)
    resp = instance_double(RestClient::Response)
    allow(resp).to receive(:body).and_return(json)
    allow_any_instance_of(RestClient::Resource).to receive(:post).and_return(resp)
    allow_any_instance_of(RestClient::Resource).to receive(:get).and_return(resp)
  end

  describe '#new' do
    before(:each) do
      @logger_double = double
      allow(@logger_double).to receive(:info)
      allow(@logger_double).to receive(:debug)
      allow_any_instance_of(described_class).to receive(:logger).and_return(@logger_double)
    end

    it 'logs "Creating client for communicating with API"' do
      expect(@logger_double).to receive(:info).once.with('Creating client for communicating with API')
      described_class.new
    end

    it 'logs key fingerprint' do
      expect(@logger_double).to receive(:debug).once.with(/Relay key fingerprint: /)
      described_class.new
    end
  end

  describe '#register' do
    it 'updates relay meta info' do
      expect(api).to receive(:update_relay_meta_info).with(JSON.parse(json))
      api.register
    end
  end

  describe '#heartbeat' do
    it 'logs the key ID' do
      fingerprint = api.instance_variable_get(:@secure_coms).key_fingerprint
      expect_any_instance_of(Relay::SecureCommunicator).to receive(:key_fingerprint).and_call_original
      expect(api.logger).to receive(:debug).ordered.and_call_original
      expect(api.logger).to receive(:debug).ordered.with("Relay key fingerprint: #{fingerprint}").and_call_original
      api.heartbeat
    end
  end

  describe '#fetch_details' do
    it 'returns the value of the "response" key' do
      expect(api.fetch_details).to eq 'valid'
    end
  end

  describe '#kill_heartbeat_thread' do
    it 'calls thread#kill' do
      thread = api.instance_variable_get(:@heartbeat_thread)
      expect(thread).to receive(:kill).and_call_original
      api.kill_heartbeat_thread
    end
  end

  describe '#queue_name' do
    it 'returns the queue name' do
      expect(api.queue_name).to eq 'norad-queue'
    end
  end

  describe '#complete_assessment' do
    it 'sends a PUT request with the proper params' do
      allow_any_instance_of(RestClient::Resource).to receive(:put)
      dummy_msg = { 'name' => 'noradjob_dummy', 'Env' => ['NORAD_SECRET=dummy'] }
      expect(api).to receive(:container_signed_put_request).with(
        'dummy',
        '/assessments/dummy',
        assessment: { state: :complete }
      ).and_call_original
      expect(api).to receive(:container_sig_header).and_call_original
      api.complete_assessment(dummy_msg)
    end
  end

  context 'ErrorManager module' do
    before(:each) do
      api.instance_variable_set(:@api_db_relay_id, '0')
    end

    describe '#create_error' do
      it 'has expected arguments' do
        expect(api).to receive(:signed_post_request).once.with(
          '/docker_relays/0/errors',
          { relay_error: { type: 'DummyKlass' },
            timestamp: Time.now.to_i.to_s },
          {}
        )
        api.create_error('DummyKlass')
      end
    end

    describe '#remove_error' do
      it 'has expected arguments' do
        expect(api).to receive(:signed_delete_request).once.with(
          '/docker_relays/0/errors/DummyKlass',
          { timestamp: Time.now.to_i.to_s },
          {}
        )
        api.remove_error('DummyKlass')
      end
    end
  end
end
