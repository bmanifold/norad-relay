# frozen_string_literal: true

require 'pstore'
require 'json'

module JSON
  class Store < PStore
    private

    def load(content)
      JSON.parse content
    rescue JSON::JSONError
      {}
    end

    def dump(table)
      JSON.dump table
    end

    EMPTY_MARSHAL_DATA = JSON.dump({})
    EMPTY_MARSHAL_CHECKSUM = Digest::MD5.digest(EMPTY_MARSHAL_DATA)

    def empty_marshal_data
      EMPTY_MARSHAL_DATA
    end

    def empty_marshal_checksum
      EMPTY_MARSHAL_CHECKSUM
    end
  end
end
