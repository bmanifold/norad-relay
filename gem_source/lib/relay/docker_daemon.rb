# frozen_string_literal: true

require 'relay/logger'
require 'relay/configuration'
require 'docker-api'

module Relay
  # Provides an interface to the Docker socket running on the host
  class DockerDaemon
    include Relay::Logger
    # Amount of time container is allowed to run. Currently set to 3 Hours
    CONTAINER_TIMEOUT = 24 * (60 * 60)

    def initialize(api)
      Docker.url = Configuration.values[:relay_host_docker_socket]
      @api = api
    end

    # Sends a message to the Docker socket
    #
    # param message [Hash] message to send to the Docker socket
    def send_to_docker(msg)
      pull_image(msg)
      container = start_container(msg)
      log_container_errors(container)
    rescue StandardError => e
      handle_exception(e)
    ensure
      remove_container(container) if ENV['RELAY_MODE'] == 'production'
      @api.complete_assessment(msg) if msg.key?('name')
    end

    def self.containers_file
      Configuration.values[:host_containers_yml_file]
    end

    def remove_dangling_images(image_name)
      Docker::Image.all(filters: { 'dangling' => ['true'] }.to_json).select do |image|
        image.info.fetch('RepoDigests', []).any? do |d|
          # We are looking at the RepoDigest since this image is pulled from a registry. Rather than
          # including a tag, it's going to have a sha256 digest appended to it, so we want to strip
          # that off.
          # Example:
          # norad-registry.cisco.com:5000/relay@sha256:5d71573af4b45f645bd8746174c258a0c1b019df1d05a0669b31e8a3c0c0df7c
          dname = d.gsub(/@sha256:[a-zA-Z0-9]{64}\z/, '')
          image_name =~ /\A#{Regexp.escape(dname)}/
        end
      end.each(&:remove)
    end

    private

    def creds_for_msg(msg)
      msg['Username'] ? { 'Username' => msg['Username'], 'Password' => msg['Password'] } : nil
    end

    def start_container(msg)
      logger.debug "Starting container for image: #{msg['Image']}..."
      container = Docker::Container.create container_opts(msg)
      container.start
      logger.debug 'Started.'
      container.wait(CONTAINER_TIMEOUT)
      logger.debug 'Container exited.'
      container
    end

    def remove_container(container)
      logger.debug 'Removing container...'
      container&.delete(force: true)
      logger.debug 'Container removed.'
    end

    def log_container_errors(container)
      container.refresh!
      if container.info.dig('State', 'ExitCode').to_s !~ /\A0\z/
        logger.error 'Container did not exit gracefully. Logs below:'
        logger.error container.streaming_logs(stdout: true, stderr: true)
      end
    rescue TypeError
      no_container_info_error(container)
    end

    def no_container_info_error(container)
      logger.error 'Unable to retrieve container exit code and logs!'
      logger.error "Container info: #{container.info.inspect}"
    end

    # Call this method before starting a container in order to ensure that the node has the latest
    # version of the image you are wanting to run
    def pull_image(msg)
      image = msg['Image']
      opts = { fromImage: image }
      creds = creds_for_msg(msg)

      logger.debug "Pulling image: #{image}"

      # Analogous to `docker pull <image_name>`
      Docker::Image.create opts, creds
      remove_dangling_images(image) if ENV['RELAY_MODE'] == 'production'
    end

    def container_opts(msg)
      file = self.class.containers_file
      aes_key = Base64.strict_encode64(CryptoUtilities.retrieve_aes_key(msg.delete('encoded_relay_private_key')))
      {
        name: msg['name'],
        Image: msg['Image'],
        Env: msg['Env'] + ["NORAD_SECURITY_CONTAINER_NAME=#{msg['Image']}", "NORAD_CONTAINERS_FILE_KEY=#{aes_key}"],
        Cmd: msg['Cmd']
      }.tap do |h|
        h[:HostConfig] = { Binds: ["#{file}:/containers.yml.enc:ro"] } if File.exist?(file)
      end
    end

    def handle_exception(e)
      logger.error 'An uncaught exception occurred while attempting to run a test'
      logger.error e.message
      logger.error e.backtrace
    end
  end
end
