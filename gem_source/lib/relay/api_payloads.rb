# frozen_string_literal: true

require 'relay/version'

module Relay
  module ApiPayloads
    def registration_payload(public_key)
      {
        organization_token: Configuration.values[:organization_token],
        docker_relay: {
          public_key: public_key
        }
      }
    end

    def assessment_update_payload
      {
        assessment: {
          state: :complete
        }
      }
    end

    def timestamp_payload
      { timestamp: Time.now.to_i.to_s }
    end

    def results_payload(results, ts = Time.now.to_i.to_s)
      { 'results[]' => results, :timestamp => ts }
    end

    def results_payload_for_signing(results, ts = Time.now.to_i.to_s)
      { results: results, timestamp: ts }
    end

    def heartbeat_payload
      { version: { current: VERSION } }
    end

    def error_payload(api_error_klass)
      {
        relay_error: {
          type: api_error_klass
        }
      }
    end
  end
end
