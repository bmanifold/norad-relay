# frozen_string_literal: true

module Relay
  module ApiAssessmentStateManager
    private

    def assessment_path(msg)
      job_name = msg.fetch('name')
      assessment_id = job_name.split('_').last
      "/assessments/#{assessment_id}"
    end
  end
end
