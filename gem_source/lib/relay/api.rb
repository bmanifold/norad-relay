# frozen_string_literal: true

require 'rest-client'
require 'json'
require 'relay/logger'
require 'relay/api_payloads'
require 'relay/api_exceptions'
require 'relay/secure_communicator'
require 'relay/store'
require 'relay/exception_logger'
require 'relay/error_manager'
require 'relay/api_http'
require 'relay/api_assessment_state_manager'

module Relay
  # Provides an interface to the API that the Relay is communicating with
  class Api
    include Relay::Logger
    include Relay::ApiPayloads
    include Relay::ExceptionLogger
    include Relay::ErrorManager
    include Relay::ApiHttp
    include Relay::ApiAssessmentStateManager
    HEARTBEAT_INTERVAL = 300
    QUEUE_SLEEP = 5

    def initialize
      logger.info 'Creating client for communicating with API'
      @store = Store.new
      @site = RestClient::Resource.new Configuration.values[:api_uri]
      @secure_coms = SecureCommunicator.new
      @api_db_relay_id = @store.fetch_meta('api_db_relay_id')
      @api_db_relay_id.nil? ? register : heartbeat
      sleep QUEUE_SLEEP
      start_beating
    end

    # Registers the Relay with the API service
    # @return [True]
    # @raises [JSON::ParserError] or [RestClient::Exception] if unsuccessful
    def register
      log_exceptions(JSON::ParserError, RestClient::Exception, SocketError) do
        resp = @site['/docker_relays'].post registration_payload(@secure_coms.encoded_public_key)
        update_relay_meta_info JSON.parse(resp.body)
        log_key_fingerprint
        true
      end
    end

    # Sends a heartbeat message to the API service
    # @return [Boolean]
    def heartbeat
      log_exceptions(RestClient::Exception, JSON::ParserError, SocketError, suppress: true) do
        logger.debug 'Sending heartbeat request to API'
        log_key_fingerprint
        resp = signed_request :post, "/docker_relays/#{@api_db_relay_id}/heartbeat", heartbeat_payload
        update_relay_meta_info JSON.parse(resp.body)
      end
    end

    # Request detail about this relay from the API
    # @raises [KeyError], [JSON::ParserError], [SocketError] or [RestClient::Exception] if unsuccessful
    def fetch_details
      log_exceptions(RestClient::Exception, JSON::ParserError, KeyError, SocketError) do
        logger.debug 'Fetching details about this relay from the API...'
        resp = signed_request :get, "/docker_relays/#{@api_db_relay_id}"
        logger.debug 'Done'
        JSON.parse(resp.body).fetch('response')
      end
    end

    # Completes an assessment associated with the container corresponding to the passed msg
    def complete_assessment(msg)
      log_exceptions(RestClient::Exception, KeyError, SocketError) do
        container_signed_put_request(container_secret(msg), assessment_path(msg), assessment_update_payload)
      end
    end

    def kill_heartbeat_thread
      @heartbeat_thread.kill
    end

    # @return [String] name of queue to connect to
    def queue_name
      @store.fetch_meta('queue_name')
    end

    private

    def log_key_fingerprint
      logger.debug "Relay key fingerprint: #{@secure_coms.key_fingerprint}"
    end

    def heartbeat_every(n)
      loop do
        before = Time.now
        heartbeat
        interval = n - (Time.now - before)
        sleep interval if interval.positive?
      end
    end

    def start_beating
      @heartbeat_thread = Thread.new do
        sleep HEARTBEAT_INTERVAL
        heartbeat_every(HEARTBEAT_INTERVAL)
      end
    end

    def update_relay_meta_info(j)
      return unless j['response']
      update_queue_name j['response']['queue_name']
      return unless j['response']['id']
      update_api_db_relay_id j['response']['id']
      @api_db_relay_id = j['response']['id']
    end

    %i[queue_name api_db_relay_id].each do |m|
      define_method "update_#{m}" do |val|
        @store.update_meta(m.to_s, val) if val
      end
    end
  end
end
