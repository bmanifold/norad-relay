# frozen_string_literal: true

require 'json'
require 'relay/logger'
require 'relay/configuration'

module Relay
  module MessageValidator
    include Relay::Logger

    # Matches any image that doesn't start with a forward slash
    IMAGE_REGEXP = %r{\A[^/].+\z}

    class << self
      # Validates the message for expected options and format
      def valid?(msg)
        return false if msg.nil? || !msg.is_a?(Hash) || msg.empty?
        logger.debug "Validating msg: #{msg.inspect}"
        valid_docker_options?(msg['docker_options'])
      end

      private

      def valid_docker_options?(opts)
        opts && valid_image?(opts['Image']) && valid_cmd?(opts['Cmd']) && valid_env?(opts['Env']) &&
          valid_creds?(opts)
      end

      def valid_image?(image)
        image && \
          image =~ IMAGE_REGEXP && \
          (docker_hub_allowed?(image) || arbitrary_host_allowed?(image))
      end

      def valid_cmd?(cmd)
        !cmd.nil?
      end

      def valid_env?(env)
        !env.nil?
      end

      # Either both or none must be provided
      def valid_creds?(opts)
        opts['Username'].nil? == opts['Password'].nil?
      end

      # Matches any string not containing a forward slash or a '.' or ':' before the first forward slash.
      # See http://stackoverflow.com/questions/37861791/how-are-docker-image-names-parsed
      # and https://github.com/docker/distribution/blob/master/reference/reference.go
      def image_from_docker_hub?(image)
        external_host_regexp = %r{\A[^/]+[.:][^/]+/.+\z}
        !image.include?('/') || image !~ external_host_regexp
      end

      def docker_hub_allowed?(image)
        image_from_docker_hub?(image) && \
          whitelisted_hosts.any? { |host| host.downcase.include?('hub.docker.com') }
      end

      def whitelisted_hosts
        Array(Configuration.values[:whitelisted_repository_hosts])
      end

      def arbitrary_host_allowed?(image)
        whitelisted_hosts.any? do |host|
          !image_from_docker_hub?(image) && \
            image.split('/').first.downcase.include?(host.downcase)
        end
      end
    end
  end
end
