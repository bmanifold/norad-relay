# frozen_string_literal: true

module Relay
  module ErrorManager
    def create_error(api_error_klass)
      signed_request(
        :post,
        "/docker_relays/#{@api_db_relay_id}/errors",
        error_payload(api_error_klass)
      )
    end

    def remove_error(api_error_klass)
      signed_request(
        :delete,
        "/docker_relays/#{@api_db_relay_id}/errors/#{api_error_klass}"
      )
    end
  end
end
