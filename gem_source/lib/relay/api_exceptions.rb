# frozen_string_literal: true

require 'rest-client'

# Requiring this file overrides some default exception messages to be more relevant to requests made to the API.
module Relay
  module WrappedExceptions
    # Ruby sometimes uses #message, while othertimes uses #to_s to print exception messages
    def message
      to_s
    end

    def to_s
      self.class::MESSAGE
    end
  end
end

class SocketError
  MESSAGE = 'Could not connect to the API! Most likely your DNS is not working properly or your network does '\
      'not allow egress access to the ports listed in the Relay documentation at '\
      'https://norad.gitlab.io/docs/#norad-relay'
  include Relay::WrappedExceptions
end

# See RestClient::Exceptions::EXCEPTIONS_MAP for full list of RestClient's HTTP code <-> Exceptions mapping
module RestClient
  # 400: Relay key or system time issue
  class BadRequest
    MESSAGE = 'There was a crypto error communicating with the API. '\
        'Ensure your system clock is up to date and the "private_key" '\
        'and "organization_token" parameters are correct in '\
        '/etc/norad.d/configuration.yml.'
    include Relay::WrappedExceptions
  end

  # 404: Relay not found when fetching details
  class NotFound
    MESSAGE = 'The requested Relay was not found in the API. '\
        'Ensure the "db" parameter in /etc/norad.d/configuration.yml points to a valid JSON store.'

    include Relay::WrappedExceptions
  end

  # 422: API returns a when registering a new relay
  class UnprocessableEntity
    MESSAGE = 'There was an error registering the Relay. '\
        'Ensure your Relay is up to date and /etc/norad.d/configuration.yml is valid.'

    include Relay::WrappedExceptions
  end

  # Lab FW may not be set up correctly
  module Exceptions
    class Timeout
      MESSAGE = 'Timeout reached while making API request. Ensure your Relay has egress access to the API '\
          'on the correct ports. See https://norad.gitlab.io/docs/#norad-relay'

      include Relay::WrappedExceptions
    end
  end

  # API returns a 500
  class InternalServerError
    MESSAGE = 'The API unexpectedly returned a 500 error. '\
        'If this issue persists please contact norad-support@cisco.com.'

    include Relay::WrappedExceptions
  end
end
