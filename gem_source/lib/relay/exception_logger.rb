# frozen_string_literal: true

module Relay
  module ExceptionLogger
    def log_exceptions(*error_klasses, **opts)
      yield
    rescue *error_klasses => e
      raise unless opts[:suppress] # relay/logger.rb logs this for us already
      logger.error e.message
      false
    end

    def report_exception(error_klass, api, api_error_klass)
      yield_returned = yield
    rescue error_klass
      api.create_error(api_error_klass)
    else
      # Remove errors if no exception was raised
      api.remove_error(api_error_klass)
      yield_returned
    end
  end
end
