# frozen_string_literal: true

module Relay
  module ApiHttp
    private

    # FIXME: GET REQUEST
    def signed_request(verb, path, payload = {}, headers = {})
      payload.merge!(timestamp_payload)
      send("signed_#{verb}_request", path, payload, headers)
    end

    def signed_post_request(path, payload = {}, headers = {})
      @site[path].post payload, headers.merge(sig_header(payload))
    end

    def container_signed_put_request(secret, path, payload = {}, headers = {})
      payload.merge!(timestamp_payload)
      @site[path].put payload, headers.merge(container_sig_header(secret, payload))
    end

    def signed_get_request(path, payload, headers = {})
      headers[:params] = payload
      @site[path].get headers.merge(sig_header(payload))
    end

    def signed_delete_request(path, payload, headers = {})
      headers[:params] = payload
      @site[path].delete headers.merge(sig_header(payload))
    end

    def sig_header(payload)
      { 'NORAD-SIGNATURE' => Base64.encode64(@secure_coms.sign(payload.to_json)).tr("\n", '') }
    end

    def container_sig_header(secret, payload)
      { 'NORAD-SIGNATURE' => @secure_coms.sign_with_secret(secret, payload.to_json) }
    end

    def container_secret(msg)
      secret_var = msg.fetch('Env').detect { |env_var| env_var.start_with?('NORAD_SECRET') }
      secret_var.gsub(/NORAD_SECRET=/, '')
    end
  end
end
